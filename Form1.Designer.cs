﻿namespace Minesweeper_6_15_2017
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.gControls = new System.Windows.Forms.GroupBox();
            this.lblDifficulty = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblMinesLeft = new System.Windows.Forms.Label();
            this.btnRestart = new System.Windows.Forms.Button();
            this.lblNA = new System.Windows.Forms.Label();
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.gameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.easy9x9ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medium18x18ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hard30x30ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timerOffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbBoard = new System.Windows.Forms.PictureBox();
            this.tClock = new System.Windows.Forms.Timer(this.components);
            this.cdSquareColor = new System.Windows.Forms.ColorDialog();
            this.gControls.SuspendLayout();
            this.msMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBoard)).BeginInit();
            this.SuspendLayout();
            //
            // gControls
            //
            this.gControls.Controls.Add(this.lblDifficulty);
            this.gControls.Controls.Add(this.lblTime);
            this.gControls.Controls.Add(this.lblMinesLeft);
            this.gControls.Controls.Add(this.btnRestart);
            this.gControls.Controls.Add(this.lblNA);
            this.gControls.Location = new System.Drawing.Point(44, 27);
            this.gControls.Name = "gControls";
            this.gControls.Size = new System.Drawing.Size(200, 100);
            this.gControls.TabIndex = 0;
            this.gControls.TabStop = false;
            //
            // lblDifficulty
            //
            this.lblDifficulty.AutoSize = true;
            this.lblDifficulty.Location = new System.Drawing.Point(88, 76);
            this.lblDifficulty.Name = "lblDifficulty";
            this.lblDifficulty.Size = new System.Drawing.Size(30, 13);
            this.lblDifficulty.TabIndex = 4;
            this.lblDifficulty.Text = "Easy";
            //
            // lblTime
            //
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.Location = new System.Drawing.Point(145, 40);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(21, 24);
            this.lblTime.TabIndex = 2;
            this.lblTime.Text = "0";
            //
            // lblMinesLeft
            //
            this.lblMinesLeft.AutoSize = true;
            this.lblMinesLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinesLeft.Location = new System.Drawing.Point(33, 40);
            this.lblMinesLeft.Name = "lblMinesLeft";
            this.lblMinesLeft.Size = new System.Drawing.Size(32, 24);
            this.lblMinesLeft.TabIndex = 1;
            this.lblMinesLeft.Text = "00";
            //
            // btnRestart
            //
            this.btnRestart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRestart.ForeColor = System.Drawing.Color.Transparent;
            this.btnRestart.Image = global::Minesweeper_6_15_2017.Properties.Resources.face22;
            this.btnRestart.Location = new System.Drawing.Point(78, 20);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(50, 50);
            this.btnRestart.TabIndex = 0;
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            //
            // lblNA
            //
            this.lblNA.AutoSize = true;
            this.lblNA.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNA.Location = new System.Drawing.Point(140, 40);
            this.lblNA.Name = "lblNA";
            this.lblNA.Size = new System.Drawing.Size(0, 24);
            this.lblNA.TabIndex = 3;
            //
            // msMain
            //
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameToolStripMenuItem});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(284, 24);
            this.msMain.TabIndex = 1;
            this.msMain.Text = "menuStrip1";
            //
            // gameToolStripMenuItem
            //
            this.gameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGameToolStripMenuItem,
            this.changeColorToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.gameToolStripMenuItem.Name = "gameToolStripMenuItem";
            this.gameToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.gameToolStripMenuItem.Text = "Game";
            //
            // newGameToolStripMenuItem
            //
            this.newGameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.easy9x9ToolStripMenuItem,
            this.medium18x18ToolStripMenuItem,
            this.hard30x30ToolStripMenuItem});
            this.newGameToolStripMenuItem.Name = "newGameToolStripMenuItem";
            this.newGameToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.newGameToolStripMenuItem.Text = "New Game";
            //
            // easy9x9ToolStripMenuItem
            //
            this.easy9x9ToolStripMenuItem.Name = "easy9x9ToolStripMenuItem";
            this.easy9x9ToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.easy9x9ToolStripMenuItem.Text = "Easy (9x9)";
            this.easy9x9ToolStripMenuItem.Click += new System.EventHandler(this.easy9x9ToolStripMenuItem_Click);
            //
            // medium18x18ToolStripMenuItem
            //
            this.medium18x18ToolStripMenuItem.Name = "medium18x18ToolStripMenuItem";
            this.medium18x18ToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.medium18x18ToolStripMenuItem.Text = "Medium (18x18)";
            this.medium18x18ToolStripMenuItem.Click += new System.EventHandler(this.medium18x18ToolStripMenuItem_Click);
            //
            // hard30x30ToolStripMenuItem
            //
            this.hard30x30ToolStripMenuItem.Name = "hard30x30ToolStripMenuItem";
            this.hard30x30ToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.hard30x30ToolStripMenuItem.Text = "Hard (27x27)";
            this.hard30x30ToolStripMenuItem.Click += new System.EventHandler(this.hard30x30ToolStripMenuItem_Click);
            //
            // changeColorToolStripMenuItem
            //
            this.changeColorToolStripMenuItem.Name = "changeColorToolStripMenuItem";
            this.changeColorToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.changeColorToolStripMenuItem.Text = "Change Colour";
            this.changeColorToolStripMenuItem.Click += new System.EventHandler(this.changeColorToolStripMenuItem_Click);
            //
            // optionsToolStripMenuItem
            //
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.timerToolStripMenuItem,
            this.timerOffToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.optionsToolStripMenuItem.Text = "Options";
            //
            // timerToolStripMenuItem
            //
            this.timerToolStripMenuItem.Name = "timerToolStripMenuItem";
            this.timerToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.timerToolStripMenuItem.Text = "Timer On";
            this.timerToolStripMenuItem.Click += new System.EventHandler(this.timerToolStripMenuItem_Click);
            //
            // timerOffToolStripMenuItem
            //
            this.timerOffToolStripMenuItem.Name = "timerOffToolStripMenuItem";
            this.timerOffToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.timerOffToolStripMenuItem.Text = "Timer Off";
            this.timerOffToolStripMenuItem.Click += new System.EventHandler(this.timerOffToolStripMenuItem_Click);
            //
            // pbBoard
            //
            this.pbBoard.Location = new System.Drawing.Point(74, 133);
            this.pbBoard.Name = "pbBoard";
            this.pbBoard.Size = new System.Drawing.Size(145, 145);
            this.pbBoard.TabIndex = 2;
            this.pbBoard.TabStop = false;
            this.pbBoard.Paint += new System.Windows.Forms.PaintEventHandler(this.pbBoard_Paint);
            this.pbBoard.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pbBoard_Click);
            //
            // tClock
            //
            this.tClock.Enabled = true;
            this.tClock.Interval = 1000;
            this.tClock.Tick += new System.EventHandler(this.tClock_Tick);
            //
            // Form1
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 361);
            this.Controls.Add(this.pbBoard);
            this.Controls.Add(this.gControls);
            this.Controls.Add(this.msMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.msMain;
            this.Name = "Form1";
            this.Text = "Minesweeper";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gControls.ResumeLayout(false);
            this.gControls.PerformLayout();
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBoard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gControls;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblMinesLeft;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem easy9x9ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem medium18x18ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hard30x30ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeColorToolStripMenuItem;
        private System.Windows.Forms.PictureBox pbBoard;
        private System.Windows.Forms.Timer tClock;
        private System.Windows.Forms.ColorDialog cdSquareColor;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timerOffToolStripMenuItem;
        private System.Windows.Forms.Label lblNA;
        private System.Windows.Forms.Label lblDifficulty;
    }
}
