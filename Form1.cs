﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Minesweeper_6_15_2017
{
    public partial class Form1 : Form
    {

        //Variables
        int iDifficulty = 1; //1=easy 2=medium 3=hard
        Color BoxColor = Color.Gray; // color to fill the boxes with
        Color BoxesOutline = Color.Black; //variable so that inverted colors can occur
        int MouseX, MouseY; //mouse vars
        int iFlagCounter = 0; //number of flags placed
        bool bFirstClick = true; //so that the clock only starts upon clicking
        bool bGameEnd = false; //when true, the mines will be drawn
        int iBoxSize = 16; //pixel size of boxes
        int iTimeElapsed = 0; //time elapsed in game

        //Arrays
        int[,] statusArray = new int[27, 27]; //0open/1closed/2flagged/3explosion
        bool[,] mineArray = new bool[27, 27]; //mine = true - no mine = false
        int[,] numberArray = new int[27, 27]; //Surrounding mines

        public Form1()
        {
            InitializeComponent();
            NewGame(iDifficulty);
        }

        //New Game method (int d = difficulty)
        public void NewGame(int d)
        {

            //Initialising variables
            bFirstClick = true;
            pbBoard.Enabled = true;
            iTimeElapsed = 0;
            lblTime.Text = "0";
            tClock.Stop();
            bGameEnd = false;
            btnRestart.Image = Properties.Resources.face22;
            btnRestart.Invalidate();


            //Variables
            int s = d * 9; //the size of the board in both directions is the difficulty times 9
            int iSpaceBetweenBoardControls = 20; //space between the board and the controls

            //Set size of the form depending on difficulty
            if (d == 1)
            {
                this.Size = new Size(300, 400);
            }
            else if (d == 2)
            {
                this.Size = new Size(500, 600);
            }
            else if (d == 3)
            {
                this.Size = new Size(700, 800);
            }

            //Set the dimensions and location of the board
            int iBoardLength = (s * 16) + 1;
            pbBoard.Size = new Size(iBoardLength, iBoardLength);
            pbBoard.Left = (this.ClientSize.Width - pbBoard.Width) / 2;
            pbBoard.Top = (this.ClientSize.Height - pbBoard.Height) / 2 + iSpaceBetweenBoardControls;

            //Set the location of the control group box
            gControls.Top = pbBoard.Top - gControls.Height;
            gControls.Left = (this.ClientSize.Width - gControls.Width) / 2;

            //Initialize the arrays
            for (int i = 0; i < s; i++)
            {
                for (int j = 0; j < s; j++)
                {
                    statusArray[i, j] = 1; //Closed by default
                    mineArray[i, j] = false; //No mines by default
                    numberArray[i, j] = 0; //^ therefore 0 for all
                }
            }

            //PLACE MINES
            //Generate mines
            Random rnd = new Random();
            //Start with 0 mines
            int iMines = 0;
            //If it is easy mode
            if (d == 1)
            {
                //10 mines for easy
                int maxMines = 10;
                lblMinesLeft.Text = maxMines.ToString();
                iFlagCounter = maxMines;
                //Loop to place mines
                while (iMines < maxMines)
                {
                    //Randomize a coordinate
                    int x = rnd.Next(0, s);
                    int y = rnd.Next(0, s);

                    //If there's no mine there, place one
                    if (!mineArray[x, y])
                    {
                        mineArray[x, y] = true;
                        iMines++;
                    }
                }
            }
            //If it is medium mode
            else if (d == 2)
            {
                int maxMines = 50; //50 mines max for medium
                lblMinesLeft.Text = maxMines.ToString();
                iFlagCounter = maxMines;
                //Loop to place mines
                while (iMines < maxMines)
                {
                    //Randomize coords
                    int x = rnd.Next(0, s);
                    int y = rnd.Next(0, s);

                    //If there is no mine, place one and increase num of mines
                    if (!mineArray[x, y])
                    {
                        mineArray[x, y] = true;
                        iMines++;
                    }
                }
            }
            //If it is hard mode
            else if (d == 3)
            {
                int maxMines = 100; //120 mines for hard mode
                lblMinesLeft.Text = maxMines.ToString();
                iFlagCounter = maxMines;
                //Loop to place mines
                while (iMines < maxMines)
                {
                    //Randomize coords
                    int x = rnd.Next(0, s);
                    int y = rnd.Next(0, s);

                    //If there is no mine there, place one
                    if (!mineArray[x, y])
                    {
                        mineArray[x, y] = true;
                        iMines++;
                    }
                }
            }

            //LOOP TO GET NUMBERS
            for (int i = 0; i < s; i++)
            {
                for (int j = 0; j < s; j++)
                {
                    //i = row, j = column
                    //0,0 is top left

                    //If there is a mine on the spot the number is -1
                    if (mineArray[i, j])
                    {
                        numberArray[i, j] = -1;
                    }
                    else
                    {
                        //Left edge
                        if (i == 0)
                        {
                            if (j == 0) //top left
                            {
                                if (mineArray[i + 1, j]) //Right
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i + 1, j + 1])//Right + Down
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i, j + 1]) //Down
                                {
                                    numberArray[i, j] += 1;
                                }
                            }
                            else if (j == (s - 1)) //bottom left
                            {
                                if (mineArray[i, j - 1]) //Left + Up
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i + 1, j - 1]) //Right + Up
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i + 1, j]) //Right
                                {
                                    numberArray[i, j] += 1;
                                }
                            }
                            else //Rest of edge
                            {
                                if (mineArray[i, j - 1]) //Up
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i, j + 1])//Down
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i + 1, j - 1])//Up + Right
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i + 1, j]) //Right
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i + 1, j + 1]) //Down + Right
                                {
                                    numberArray[i, j] += 1;
                                }
                            }
                        }

                        //Right edge
                        else if (i == s - 1)
                        {
                            if (j == 0) //top right
                            {
                                if (mineArray[i - 1, j + 1])
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i - 1, j])
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i, j + 1])
                                {
                                    numberArray[i, j] += 1;
                                }
                            }
                            else if (j == s - 1) //bottom right
                            {
                                if (mineArray[i - 1, j - 1])
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i - 1, j])
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i, j - 1])
                                {
                                    numberArray[i, j] += 1;
                                }
                            }
                            else
                            {
                                if (mineArray[i, j - 1])
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i, j + 1])
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i - 1, j - 1])
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i - 1, j])
                                {
                                    numberArray[i, j] += 1;
                                }
                                if (mineArray[i - 1, j + 1])
                                {
                                    numberArray[i, j] += 1;
                                }
                            }
                        }

                        //Top edge
                        else if (j == 0 && i != 0 && i != s - 1)
                        {
                            if (mineArray[i + 1, j])
                            {
                                numberArray[i, j]++;
                            }
                            if (mineArray[i + 1, j + 1])
                            {
                                numberArray[i, j]++;
                            }
                            if (mineArray[i - 1, j])
                            {
                                numberArray[i, j]++;
                            }
                            if (mineArray[i - 1, j + 1])
                            {
                                numberArray[i, j]++;
                            }
                            if (mineArray[i, j + 1])
                            {
                                numberArray[i, j]++;
                            }
                        }

                        //Bottom edge
                        else if (j == s - 1 && i != 0 && i != s - 1)
                        {
                            if (mineArray[i - 1, j])
                            {
                                numberArray[i, j] += 1;
                            }
                            if (mineArray[i + 1, j])
                            {
                                numberArray[i, j] += 1;
                            }
                            if (mineArray[i - 1, j - 1])
                            {
                                numberArray[i, j] += 1;
                            }
                            if (mineArray[i + 1, j - 1])
                            {
                                numberArray[i, j] += 1;
                            }
                            if (mineArray[i, j - 1])
                            {
                                numberArray[i, j] += 1;
                            }
                        }

                        //Rest of it
                        else
                        {
                            if (mineArray[i - 1, j + 1])
                            {
                                numberArray[i, j]++;
                            }
                            if (mineArray[i - 1, j])
                            {
                                numberArray[i, j]++;
                            }
                            if (mineArray[i - 1, j - 1])
                            {
                                numberArray[i, j]++;
                            }
                            if (mineArray[i, j - 1])
                            {
                                numberArray[i, j]++;
                            }
                            if (mineArray[i, j + 1])
                            {
                                numberArray[i, j]++;
                            }
                            if (mineArray[i + 1, j + 1])
                            {
                                numberArray[i, j]++;
                            }
                            if (mineArray[i + 1, j])
                            {
                                numberArray[i, j]++;
                            }
                            if (mineArray[i + 1, j - 1])
                            {
                                numberArray[i, j]++;
                            }
                        }
                    }
                }
            }
        }

        //Events
            //menu strip
        private void easy9x9ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewGame(1);
            iDifficulty = 1;
            lblDifficulty.Text = "Easy";
            pbBoard.Invalidate();
        }
        private void medium18x18ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewGame(2);
            iDifficulty = 2;
            lblDifficulty.Text = "Medium";
            pbBoard.Invalidate();
        }
        private void hard30x30ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewGame(3);
            iDifficulty = 3;
            lblDifficulty.Text = "Hard";
            pbBoard.Invalidate();
        }
        private void changeColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cdSquareColor.ShowDialog();
            BoxColor = cdSquareColor.Color;
            if (BoxColor == Color.Black)
            {
                BoxesOutline = Color.Gray;
            }
            pbBoard.Invalidate();
        }
        private void timerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lblTime.Visible = true;
            lblNA.Visible = false;
        }
        private void timerOffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lblTime.Visible = false;
            lblNA.Text = "NA";
            lblNA.Visible = true;
        }
            //clock tick
        private void tClock_Tick(object sender, EventArgs e)
        {
            iTimeElapsed++;
            lblTime.Text = iTimeElapsed.ToString();
        }
            //restart button
        private void btnRestart_Click(object sender, EventArgs e)
        {
            NewGame(iDifficulty);
            pbBoard.Invalidate();
        }
            //board events
        private void pbBoard_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            //Variables
            int numOfRows = (pbBoard.Width - 1) / 16;
            int numOfColumns = (pbBoard.Height - 1) / 16;

            //Pens
            Pen pOutline = new Pen(Color.Black, 2); //draws board
            Pen pBoxes = new Pen(BoxesOutline, 2); //draws boxes

            //Fonts
            Font fStatus = new Font(FontFamily.GenericSansSerif, 11.0F);

            //Brushes
            SolidBrush sbStatus = new SolidBrush(Color.Black); //draws numbers
            SolidBrush sbBoxFill = new SolidBrush(BoxColor);

            //Indicates the end of the board, just before the actual end
            int iWidth = pbBoard.Width - 1;
            int iHeight = pbBoard.Height - 1;

            //Call on the drawboard method with the above drawing tools
            DrawBoard(g, sbStatus, fStatus, pBoxes, sbBoxFill);

            //If it is the endgame, call the appropriate method
            if (bGameEnd)
            {
                EndMines(g, pBoxes, sbBoxFill);
            }

        }
        private void pbBoard_Click(object sender, MouseEventArgs e)
        {
            MouseX = e.X;
            MouseY = e.Y;

            //On the first click, start the clock and set firstclick to false
            if (bFirstClick)
            {
                tClock.Start();
                bFirstClick = false;
            }

            //Call reveal method on left click
            if (e.Button == MouseButtons.Left)
            {
                Reveal(MouseX, MouseY);
            }
            //flag on right click
            if (e.Button == MouseButtons.Right)
            {
                Flag(MouseX, MouseY);
            }
            //If the game has been won, end it
            if (WinCondition())
            {
                tClock.Stop();
                pbBoard.Invalidate();
                pbBoard.Enabled = false;
                MessageBox.Show("Congratulations! You won in " + iTimeElapsed + " seconds!");
                return;
            }
        }

        //Other Methods
        public void openAround(int r, int c)
        {
            int numOfRows = (pbBoard.Height - 1) / 16;
            int numOfColumns = (pbBoard.Width - 1) / 16;
            int maxR = numOfRows - 1;
            int maxC = numOfColumns - 1;

            if (r < 0 || r > maxR || c < 0 || c > maxC)
            {
                return;
            }

            if (numberArray[r, c] == 0 && statusArray[r, c] == 1)
            {

                statusArray[r, c] = 0;

                openAround(r - 1, c + 1);
                openAround(r - 1, c);
                openAround(r - 1, c - 1);
                openAround(r, c + 1);
                openAround(r, c - 1);
                openAround(r + 1, c + 1);
                openAround(r + 1, c);
                openAround(r + 1, c - 1);

                pbBoard.Invalidate();
            }
            else if (numberArray[r, c] > 0 && statusArray[r, c] == 1)
            {
                statusArray[r, c] = 0;
                return;
            }
        }
        public void Reveal(int x, int y)
        {
            //Main Loop, reveals box
            for (int i = 0; i < (pbBoard.Width - 1) / 16; i++)
            {
                //The current column is 16*number of loops
                int startX = iBoxSize * i;

                for (int j = 0; j < (pbBoard.Height - 1) / 16; j++)
                {
                    //The current row is 16*number of inside loops
                    int startY = iBoxSize * j;
                    int width = startX + iBoxSize;
                    int height = startY + iBoxSize;

                    //Must be between the beginning of the row/column and the end of it
                    if (x > startX && x < width && y > startY && y < height)
                    {
                        if (statusArray[i, j] != 2)
                        {
                            //If there is a mine on that spot, end the game
                            if (mineArray[i, j])
                            {
                                Gameover(i, j); //calls on the Gameover method
                                continue; //Skip the rest of the loop
                            }

                            //If a 0 is revealed, expand as much as possible
                            else if (numberArray[i, j] == 0)
                            {
                                openAround(i, j);
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                        //Set the status to open and redraw the board
                        statusArray[i, j] = 0;
                        pbBoard.Invalidate();
                    }
                }
            }
        }
        public void Gameover(int r, int c)
        {
            //Set the tile to explosion and re-draw the board
            statusArray[r, c] = 3;

            //Set the face at the top to the angry version
            btnRestart.Image = Properties.Resources.face3_angry;
            btnRestart.Invalidate();

            pbBoard.Invalidate();
            pbBoard.Enabled = false;
            tClock.Stop();
        }
        public void Flag(int x, int y)
        {
            //Get graphics from the board
            Graphics g = Graphics.FromHwnd(pbBoard.Handle);

            //Pens & Brushes
            Pen pen = new Pen(Color.LightGreen, 2);
            SolidBrush br = new SolidBrush(Color.LightGreen);

            //Main Loop, reveals box
            for (int i = 0; i < (pbBoard.Width - 1) / 16; i++)
            {
                //The current column is 16*number of loops
                int startX = iBoxSize * i;

                for (int j = 0; j < (pbBoard.Height - 1) / 16; j++)
                {
                    //The current row is 16*number of inside loops
                    int startY = iBoxSize * j;
                    int width = startX + iBoxSize;
                    int height = startY + iBoxSize;

                    bool boxIsClicked = x > startX && x < width && y > startY && y < height;
                    //0open/1closed/2flagged/3explosion
                    if (boxIsClicked && statusArray[i, j] != 0)
                    {
                        if (statusArray[i, j] != 2)
                        {
                            statusArray[i, j] = 2;
                            pbBoard.Invalidate();
                            iFlagCounter--;
                            lblMinesLeft.Text = iFlagCounter.ToString();
                        }
                        else if (statusArray[i, j] == 2)
                        {
                            statusArray[i, j] = 1;
                            pbBoard.Invalidate();
                            iFlagCounter++;
                            lblMinesLeft.Text = iFlagCounter.ToString();
                        }
                    }
                    else
                        continue;
                }
            }
        }
        public bool WinCondition()
        {
            //0open/1closed/2flagged/3explosion
            //number of unopened square has to be equal to the number of mines
            for (int i = 0; i < (pbBoard.Width - 1) / 16; i++)
            {
                for (int j = 0; j < (pbBoard.Height - 1) / 16; j++)
                {
                    if (statusArray[i, j] == 0 && mineArray[i, j] == false)
                    {
                        continue;
                    }
                    else if ((statusArray[i, j] == 1 || statusArray[i, j] == 2) && mineArray[i, j] == true)
                    {
                        continue;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public void EndMines(Graphics g, Pen pBoxes, SolidBrush sbBoxFill)
        {
            //Draws where mines were and were incorrect flags were at end of game
            int numOfRows = (pbBoard.Width - 1) / 16;
            int numOfColumns = (pbBoard.Height - 1) / 16;

            for (int k = 0; k < numOfRows; k++)
            {
                int startPosX1 = 16 * k;
                for (int s = 0; s < numOfColumns; s++)
                {
                    int startPosY1 = 16 * s;

                    //If there is a mine and it is not the exploding one, draw it
                    if (mineArray[k, s])
                    {
                        //If it was flagged & there was a mine, they were correct
                        if (statusArray[k, s] == 2)
                        {
                            //If it is the inverse version (color selected was black) draw a visible flag
                            if (BoxColor == Color.Black)
                            {
                                g.DrawImage(Properties.Resources.GrayFlag, startPosX1, startPosY1);
                            }
                            //Otherwise draw the regular flag
                            else
                            {
                                g.DrawImage(Properties.Resources.Flag4124, startPosX1, startPosY1);
                            }
                        }

                        //If there was a mine and no flag, draw a mine
                        else
                        {
                            //Inverted colour version
                            if (BoxColor == Color.Black)
                            {
                                g.DrawImage(Properties.Resources.GrayMine, startPosX1, startPosY1);
                            }
                            //Normal version
                            else
                            {
                                g.DrawImage(Properties.Resources.NormalMine1, startPosX1, startPosY1);
                            }
                        }
                    }

                    //If there was no mine but they flagged it, they were wrong
                    else if (!mineArray[k, s] && statusArray[k, s] == 2)
                    {
                        //draw a mine with an x on it
                        g.DrawImage(Properties.Resources.WrongFlag, startPosX1, startPosY1);
                    }
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public void DrawBoard(Graphics g, SolidBrush sbStatus, Font fStatus, Pen pBoxes, SolidBrush sbBoxFill)
        {
            //Logic for drawing the board goes here

            int numOfRows = (pbBoard.Width - 1) / 16;
            int numOfColumns = (pbBoard.Height - 1) / 16;

            for (int i = 0; i < numOfRows; i++) //Rows
            {
                //The x coord of the current box is 16 times the column of the box
                int startPosX = 16 * i;

                for (int j = 0; j < numOfColumns; j++) //Columns
                {
                    //The y coord of the current box is 16 times the row of the box
                    int startPosY = 16 * j;

                    //Combine x and y into a usable point
                    Point pos = new Point(startPosX, startPosY);

                    //Convert status to a string so it can be drawn
                    string stat = Convert.ToString(numberArray[i, j]);

                    //Draw different numbers with different colors
                    if (numberArray[i, j] == 1)
                        sbStatus.Color = Color.Blue;
                    if (numberArray[i, j] == 2)
                        sbStatus.Color = Color.Green;
                    if (numberArray[i, j] == 3)
                        sbStatus.Color = Color.Red;
                    if (numberArray[i, j] == 4)
                        sbStatus.Color = Color.DarkBlue;
                    if (numberArray[i, j] == 5)
                        sbStatus.Color = Color.DarkRed;
                    if (numberArray[i, j] == 6)
                        sbStatus.Color = Color.Turquoise;
                    if (numberArray[i, j] == 7)
                        sbStatus.Color = Color.Black;
                    if (numberArray[i, j] == 8)
                        sbStatus.Color = Color.Gray;

                    //If the box is open and it is not a zero, write the number
                    if (numberArray[i, j] != 0 && statusArray[i, j] == 0)
                        g.DrawString(stat, fStatus, sbStatus, pos);
                    //Draw the outline of the box
                    g.DrawRectangle(pBoxes, startPosX, startPosY, 16, 16);

                    //0open/1closed/2flagged/3explosion
                    if (statusArray[i, j] == 1) //If it is closed
                    {
                        //Draw the box
                        g.DrawRectangle(pBoxes, startPosX, startPosY, 16, 16);
                        g.FillRectangle(sbBoxFill, startPosX, startPosY, 16, 16);
                    }
                    else if (statusArray[i, j] == 2)
                    {
                        g.DrawRectangle(pBoxes, startPosX, startPosY, 16, 16);
                        g.FillRectangle(sbBoxFill, startPosX, startPosY, 16, 16);
                        if (BoxColor == Color.Black)
                        {
                            g.DrawImage(Properties.Resources.GrayFlag, startPosX, startPosY);
                        }
                        else
                        {
                            g.DrawImage(Properties.Resources.Flag4124, startPosX, startPosY);
                        }
                        continue;
                    }

                    else if (statusArray[i, j] == 3) //If there was a mine there (game ended)
                    {
                        //Draws the exploding mine (mine with red background)
                        Bitmap ExplodingMine = new Bitmap("ExplodingMine1.bmp");
                        g.DrawImage(ExplodingMine, startPosX, startPosY);
                        bGameEnd = true;
                    }

                }
            }
        }
    }
}
